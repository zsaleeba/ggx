# ggx

GreenGrass eXecute tool.

An easy way for developers to run command line programs under greengrass permissions. Bypasses the awkwardness of not being able to use the command line to work with greengrass programs.


eg.
```
$ ggx gdb my_greengrass_component
```

Debugs your component with access to greengrass. You don't need to have configured it as a greengrass component yet.

```
$ ggx ggipc_sub -t my/topic
```

If you have a program ggipc_sub which subscribes to a greengrass IPC topic and allows you to run it with access to greengrass IPC.


## Building

Just type `make`. Build artefacts are in `build`.
