//
// A client class which can be used to execute programs within the greengrass environment.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#ifndef GGXCLIENT_HPP
#define GGXCLIENT_HPP

#include <string>
#include <vector>
#include <map>
#include <cerrno>
#include <unistd.h>

#include "ggxclientconn.hpp"


namespace ggx
{


//
// A client class which can be used to execute programs within the greengrass environment.
//

class Client
{
private:
    static constexpr const size_t IO_BUF_SIZE = 65536;

public:
    // Constructor. Directs I/O to the provided fds.
    explicit Client();

    // Executes a program inside greengrass.
    // Returns 0 on success or a standard errno otherwise.
    int exec(const std::string &command, const std::vector<std::string> &args, const std::map<std::string, std::string> &env, const std::string &pwd);

    // Copy I/O until the program terminates.
    // Returns the program's return value.
    int copy_io(int &return_code, int stdin_fd = STDIN_FILENO, int stdout_fd = STDOUT_FILENO, int stderr_fd = STDERR_FILENO);

private:
    // The socket connection to the ggserver.
    ClientConn m_conn;
};


} // namespace ggx


#endif // GGXCLIENT_HPP
