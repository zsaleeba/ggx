//
// A socket connection and protocol for connecting to the ggxserver.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#ifndef GGXCLIENTCONN_HPP
#define GGXCLIENTCONN_HPP

#include <string>

#include "ggxproto.hpp"


namespace ggx
{


//
// A socket connection and protocol for connecting to the ggxserver.
//

class ClientConn
{
public:
    // The port the ggserver listens on.
    static constexpr const int GGX_SERVER_PORT = 16333;
    static constexpr const size_t IO_BUF_SIZE = 65536;

public:
    // Constructor.
    explicit ClientConn();
    virtual ~ClientConn();

    // Connect to the ggserver.
    int connect();

    // Check if we're connected.
    bool is_connected() const;

    // Send messages.
    int send_exec_request(const std::string &command, const std::vector<std::string> &args, const std::map<std::string, std::string> &env, const std::string &pwd);
    int send_exec_response();
    int send_stdin_data(const std::string &data);
    int send_stdout_data(const std::string &data);
    int send_stderr_data(const std::string &data);
    int send_program_terminated(int return_code);

    // Send and receive I/O. Returns when a message is received.
    int receive_message(Proto::MsgType &decoded_msg_type, std::vector<uint8_t> &decoded_msg, int stdin_fd, bool &got_stdin);

    // Decode a program terminated message.
    bool decode_program_terminated(const std::vector<uint8_t> &msg_data, int &return_code) { return m_proto.decode_program_terminated(msg_data, return_code); }

private:
    // Send the transmit buffer.
    int send_tx_buffer();

private:
    // The socket connection to the gg server.
    int m_socket_fd;

    // The protocol.
    Proto m_proto;
};


} // namespace ggx



#endif // GGXCLIENTCONN_HPP
