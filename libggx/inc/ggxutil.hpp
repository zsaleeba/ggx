//
// Utilities used by libggx.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#ifndef GGXUTIL_HPP
#define GGXUTIL_HPP

#include <string>
#include <vector>
#include <map>
#include <cerrno>
#include <unistd.h>

#include "ggxclientconn.hpp"


namespace ggx
{


// Make an fd non-blocking.
int set_nonblocking(int fd);


} // namespace ggx


#endif // GGXUTIL_HPP
