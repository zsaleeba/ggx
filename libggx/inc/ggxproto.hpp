//
// A protocol for communicating between client and ggxserver.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#ifndef GGXPROTO_HPP
#define GGXPROTO_HPP

#include <string>
#include <vector>
#include <map>
#include <mutex>
#include <cstdio>
#include <cstdint>


namespace ggx
{


//
// A protocol for communicating between client and ggserver.
//

class Proto
{
public:
    // The different message types.
    enum class MsgType
    {
        NONE = 0,                   // Not an actual message type, used internally.
        EXEC_REQ,
        EXEC_RESP,
        STDIN_DATA,
        STDOUT_DATA,
        STDERR_DATA,
        TERMIO_SETTING,
        RAISE_SIGNAL,
        PROGRAM_TERMINATED,
        CONNECTION_CLOSED           // Not an actual message type, used internally.
    };

    // The current protocol version.
    static constexpr const int PROTOCOL_VERSION = 1;

    // If the transmit buffer gets this full limit the data we send.
    static constexpr const size_t TX_BUF_HIGH_WATER = 65536;

public:
    // Constructor.
    explicit Proto();

    // Add received data from the socket to the receive buffer.
    void add_rx_data(const uint8_t *data, size_t len);

    // Access transmit buffer data for sending.
    // Use tx_data() and tx_data_len() to get a buffer to send.
    // Then once sent use pop_tx_data() to remove it from the buffer.
    const uint8_t *tx_data()     const { return m_tx_buf.data(); }
    size_t         tx_data_len() const { return m_tx_buf.size(); }
    void           pop_tx_data(size_t len);

    // Encodes a message.
    // Appends the encoded data to m_tx_buf.
    void encode(MsgType msg_type, const std::vector<uint8_t> &payload);
    void encode(MsgType msg_type, const std::string &payload);

    // Decodes a message.
    // Will remove the read data from the receive buffer.
    // Returns the decoded message type and data via decoded_msg_type and decoded_msg.
    // Returns true if a message was decoded, false otherwise.
    bool decode(MsgType &decoded_msg_type, std::vector<uint8_t> &decoded_msg);

    // Encode specific message types.
    void encode_exec_request(const std::string &command, const std::vector<std::string> &args, const std::map<std::string, std::string> &env, const std::string &pwd, bool isatty);
    void encode_exec_response();
    void encode_stdin_data(const std::string &data);
    void encode_stdout_data(const std::string &data);
    void encode_stderr_data(const std::string &data);
    void encode_raise_signal(int return_code);
    void encode_program_terminated(int return_code);

    // Decode specific message types.
    bool decode_exec_request(const std::vector<uint8_t> &msg_data, std::string &command, std::vector<std::string> &args, std::map<std::string, std::string> &env, std::string &pwd, bool &isatty);
    bool decode_raise_signal(const std::vector<uint8_t> &msg_data, int &return_code);
    bool decode_program_terminated(const std::vector<uint8_t> &msg_data, int &return_code);

private:
    // Transmit buffer.
    std::vector<uint8_t> m_tx_buf;
    std::mutex           m_tx_mutex;

    // Receive buffer.
    std::vector<uint8_t> m_rx_buf;
    std::mutex           m_rx_mutex;
};


} // namespace ggx



#endif // GGXPROTO_HPP
