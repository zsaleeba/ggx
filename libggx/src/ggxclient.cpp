//
// A client class which can be used to execute programs within the greengrass environment.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>

#include "ggxclient.hpp"
#include "ggxutil.hpp"


namespace ggx
{


//
// Constructor. Directs I/O to the provided fds.
//

Client::Client()
{
}


//
// Executes a program inside greengrass.
//

int Client::exec(const std::string &command, const std::vector<std::string> &args, const std::map<std::string, std::string> &env, const std::string &pwd)
{
    // Connect if we're not already connected.
    if (!m_conn.is_connected())
    {
        int rc = m_conn.connect();
        if (rc != 0)
            return rc;
    }

    // Send the exec request.
    int rc = m_conn.send_exec_request(command, args, env, pwd);
    if (rc != 0)
        return rc;

    // Wait for the exec response.
    Proto::MsgType decoded_msg_type = Proto::MsgType::NONE;
    std::vector<uint8_t> decoded_msg;
    bool got_stdin = false;
    rc = 0;
    while ( (rc = m_conn.receive_message(decoded_msg_type, decoded_msg, -1, got_stdin)) == 0 && (decoded_msg_type != Proto::MsgType::EXEC_RESP && decoded_msg_type != Proto::MsgType::CONNECTION_CLOSED) )
    {
    }

    if (rc != 0)
        return rc;

    if (decoded_msg_type == Proto::MsgType::CONNECTION_CLOSED)
        return EISCONN;
    
    return 0;
}


//
// Copy I/O until the program terminates.
// return_code gets the program's return value.
// Returns 0 on success or an errno error.
//

int Client::copy_io(int &return_code, int stdin_fd, int stdout_fd, int stderr_fd)
{
    // Make stdin non-blocking so we don't wait on it.
    int rc = set_nonblocking(stdin_fd);
    if (rc != 0)
        return rc;

    // Handle messages until the program terminates.
    Proto::MsgType decoded_msg_type = Proto::MsgType::NONE;
    std::vector<uint8_t> decoded_msg;
    bool got_stdin = false;
    uint8_t io_buf[IO_BUF_SIZE];
    return_code = 0;

    while ( (rc = m_conn.receive_message(decoded_msg_type, decoded_msg, stdin_fd, got_stdin)) == 0)
    {
        // Handle a received message.
        switch (decoded_msg_type)
        {
        case Proto::MsgType::NONE:
        case Proto::MsgType::EXEC_REQ:
        case Proto::MsgType::EXEC_RESP:
        case Proto::MsgType::STDIN_DATA:
            break;

        case Proto::MsgType::STDOUT_DATA:
            write(stdout_fd, decoded_msg.data(), decoded_msg.size());
            break;

        case Proto::MsgType::STDERR_DATA:
            write(stderr_fd, decoded_msg.data(), decoded_msg.size());
            break;

        case Proto::MsgType::TERMIO_SETTING:
            break;

        case Proto::MsgType::RAISE_SIGNAL:
            break;

        case Proto::MsgType::PROGRAM_TERMINATED:
            if (m_conn.decode_program_terminated(decoded_msg, return_code))
                return 0;
            break;

        case Proto::MsgType::CONNECTION_CLOSED:
            return EISCONN;
        }

        // Handle some input on stdin.
        if (got_stdin)
        {
            // Read from stdin.
            ssize_t read_bytes = read(stdin_fd, io_buf, sizeof(io_buf));
            if (read_bytes < 0)
                return errno;
            
            // Send it via the socket.
            if (read_bytes > 0)
            {
                rc = m_conn.send_stdin_data(std::string(io_buf, io_buf + read_bytes));
                if (rc != 0)
                    return rc;
            }
        }
    }

    if (rc != 0)
        return rc;

    return 0;
}



} // namespace ggx
