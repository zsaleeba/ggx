//
// A protocol for communicating between client and ggxserver.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#include <string>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <cstdint>
#include <nlohmann/json.hpp>

#include "ggxproto.hpp"


using json = nlohmann::json;



namespace ggx
{




Proto::Proto()
{
}


//
// Add received data to the receive buffer.
//

void Proto::add_rx_data(const uint8_t *data, size_t len)
{
    std::lock_guard<std::mutex> lock(m_rx_mutex);
    m_rx_buf.insert(m_rx_buf.end(), data, data + len);
}



//
// Remove tx data from the buffer after sending.
//

void Proto::pop_tx_data(size_t len)
{
    std::lock_guard<std::mutex> lock(m_tx_mutex);
    m_tx_buf.erase(m_tx_buf.begin(), m_tx_buf.begin() + len);
}



// Encodes a message.
// Appends the encoded data to m_tx_buf.
void Proto::encode(MsgType msg_type, const std::vector<uint8_t> &payload)
{
    std::lock_guard<std::mutex> lock(m_tx_mutex);
    uint32_t msg_size = sizeof(uint32_t) + sizeof(uint8_t) + payload.size();

    // Write the message length.
    uint8_t *msg_size_bytes = reinterpret_cast<uint8_t *>(&msg_size);
    m_tx_buf.insert(m_tx_buf.end(), msg_size_bytes, msg_size_bytes + sizeof(msg_size));

    // Write the message type.
    m_tx_buf.push_back(static_cast<uint8_t>(msg_type));

    // Now write the message payload.
    m_tx_buf.insert(m_tx_buf.end(), payload.data(), payload.data() + payload.size());
}


void Proto::encode(MsgType msg_type, const std::string &payload)
{
    std::vector<uint8_t> payload_vec(payload.begin(), payload.end());
    encode(msg_type, payload_vec);
}


// Decodes a message.
// Will remove the read data from the receive buffer.
// Returns the decoded message type and data via decoded_msg_type and decoded_msg.
// Returns true if a message was decoded, false otherwise.
bool Proto::decode(MsgType &decoded_msg_type, std::vector<uint8_t> &decoded_msg)
{
    std::lock_guard<std::mutex> lock(m_rx_mutex);

    // Do we have enough bytes to decode?
    size_t header_size = sizeof(uint32_t) + sizeof(uint8_t);
    if (m_rx_buf.size() < header_size)
        return false;

    // Read the message length.
    auto msg_size = *reinterpret_cast<uint32_t *>(m_rx_buf.data());
    if (m_rx_buf.size() < header_size + msg_size)
        return false;

    // Read the message type.
    decoded_msg_type = static_cast<MsgType>(m_rx_buf.data()[sizeof(uint32_t)]);

    // Now copy the payload out.
    size_t payload_size = msg_size - header_size;
    decoded_msg.resize(payload_size);
    std::copy(m_rx_buf.begin() + header_size, m_rx_buf.end(), decoded_msg.begin());

    return true;
}


//
// Encode an exec request.
//

void Proto::encode_exec_request(const std::string &command, const std::vector<std::string> &args, const std::map<std::string, std::string> &env, const std::string &pwd, bool isatty)
{
    // Create a message.
    json msg = { 
        { "command", command },
        { "args", args },
        { "env", env },
        { "pwd", pwd },
        { "isatty", isatty },
        { "protocol_version", PROTOCOL_VERSION }
    };

    encode(MsgType::EXEC_REQ, msg.dump());
}


//
// Decode an exec request.
// The message is passed in msg_data.
// Results are passed back in the remaining parameters.
// Returns true on success, false on failure.
//

bool Proto::decode_exec_request(const std::vector<uint8_t> &msg_data, std::string &command, std::vector<std::string> &args, std::map<std::string, std::string> &env, std::string &pwd, bool &isatty)
{
    // Clear the return values.
    command.clear();
    args.clear();
    env.clear();
    pwd.clear();
    isatty = false;

    // Parse them.
    try {
        json j = json::parse(msg_data);
        command = j["command"].get<std::string>();
        args = j["args"].get< std::vector<std::string> >();
        env = j["env"].get< std::map<std::string, std::string> >();
        pwd = j["pwd"].get<std::string>();
        isatty = j["pwd"].get<bool>();
        int protocol_version = j["protocol_version"].get<int>();
        if (protocol_version != PROTOCOL_VERSION)
            return false;
    }
    catch (const json::parse_error &e) {
        return false;
    }

    return true;
}


//
// Encode an exec response.
//

void Proto::encode_exec_response()
{
    std::vector<uint8_t> msg;
    encode(MsgType::EXEC_RESP, msg);
}


//
// Encode some stdin data.
//

void Proto::encode_stdin_data(const std::string &data)
{
    std::vector<uint8_t> dvec(data.begin(), data.end());
    encode(MsgType::STDIN_DATA, dvec);
}


//
// Encode some stdout data.
//

void Proto::encode_stdout_data(const std::string &data)
{
    std::vector<uint8_t> dvec(data.begin(), data.end());
    encode(MsgType::STDOUT_DATA, dvec);
}


//
// Encode some stderr data.
//

void Proto::encode_stderr_data(const std::string &data)
{
    std::vector<uint8_t> dvec(data.begin(), data.end());
    encode(MsgType::STDERR_DATA, dvec);
}


//
// Encode a raise signal message.
//

void Proto::encode_raise_signal(int signal_id)
{
    // Create a message.
    json msg = { 
        { "signal_id", signal_id }
    };

    encode(MsgType::RAISE_SIGNAL, msg.dump());
}


//
// Decode a raise signal message.
// The message is passed in msg_data.
// Result is passed back in signal_id.
// Returns true on success, false on failure.
//

bool Proto::decode_raise_signal(const std::vector<uint8_t> &msg_data, int &signal_id)
{
    // Clear the return values.
    signal_id = -1;

    try {
        json j = json::parse(msg_data);
        signal_id = j["signal_id"].get<int>();
    }
    catch (const json::parse_error &e) {
        return false;
    }

    return true;
}


//
// Encode an program terminated message.
//

void Proto::encode_program_terminated(int return_code)
{
    // Create a message.
    json msg = { 
        { "return_code", return_code }
    };

    encode(MsgType::PROGRAM_TERMINATED, msg.dump());
}


//
// Decode a program terminated message.
// The message is passed in msg_data.
// Result is passed back in return_code.
// Returns true on success, false on failure.
//

bool Proto::decode_program_terminated(const std::vector<uint8_t> &msg_data, int &return_code)
{
    // Clear the return values.
    return_code = -1;

    try {
        json j = json::parse(msg_data);
        return_code = j["return_code"].get<int>();
    }
    catch (const json::parse_error &e) {
        return false;
    }

    return true;
}


} // namespace ggx
