//
// A socket connection and protocol for connecting to the ggxserver.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <string>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <poll.h>
#include <fcntl.h>

#include "ggxclientconn.hpp"


namespace ggx
{


// Constructor. Directs I/O to the provided fds.
ClientConn::ClientConn()
    : m_socket_fd(-1)
{

}


ClientConn::~ClientConn()
{
    if (m_socket_fd >= 0)
    {
        ::close(m_socket_fd);
    }
}


//
// Connects to the server.
// Returns 0 on success or a standard errno on failure.
//

int ClientConn::connect()
{
    // Create a socket.
    m_socket_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if (m_socket_fd < 0)
        return errno;

    // Make a socket connection to the server.
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(GGX_SERVER_PORT);
    if (inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) == 0) 
        return errno;
    
    if (::connect(m_socket_fd, reinterpret_cast<sockaddr *>(&server_addr), sizeof(server_addr)) < 0)
        return errno;
    
    return 0;
}


//
// Check if the socket's connected.
//

bool ClientConn::is_connected() const
{
    return m_socket_fd >= 0;
}



//
// Send the transmit buffer.
// Returns 0 on success or an errno on error.
//

int ClientConn::send_tx_buffer()
{
    ssize_t written = write(m_socket_fd, m_proto.tx_data(), m_proto.tx_data_len());
    if (written < 0)
        return errno;

    if (written > 0)
    {
        m_proto.pop_tx_data(written);
    }

    return 0;
}


//
// Send messages of various types.
//

int ClientConn::send_exec_request(const std::string &command, const std::vector<std::string> &args, const std::map<std::string, std::string> &env, const std::string &pwd)
{
    m_proto.encode_exec_request(command, args, env, pwd);
    return send_tx_buffer();
}


int ClientConn::send_exec_response()
{
    m_proto.encode_exec_response();
    return send_tx_buffer();
}


int ClientConn::send_stdin_data(const std::string &data)
{
    m_proto.encode_stdin_data(data);
    return send_tx_buffer();
}


int ClientConn::send_stdout_data(const std::string &data)
{
    m_proto.encode_stdout_data(data);
    return send_tx_buffer();
}


int ClientConn::send_stderr_data(const std::string &data)
{
    m_proto.encode_stderr_data(data);
    return send_tx_buffer();
}


int ClientConn::send_program_terminated(int return_code)
{
    m_proto.encode_program_terminated(return_code);
    return send_tx_buffer();
}


//
// Send and receive I/O. Returns 0 when a message is received.
// Returns an errno on error.
// If the connection is closed returns decoded_msg_type = CONNECTION_CLOSED.
//

int ClientConn::receive_message(Proto::MsgType &decoded_msg_type, std::vector<uint8_t> &decoded_msg, int stdin_fd, bool &got_stdin)
{
    // Clear the results.
    decoded_msg_type = Proto::MsgType::NONE;
    decoded_msg.clear();
    got_stdin = false;

    // Check if there's a message ready to be decoded.
    if (m_proto.decode(decoded_msg_type, decoded_msg))
        return 0;

    // Keep getting data until we have a packet.
    while (true)
    {
        // What events are we waiting on?
        std::vector<pollfd> fds;

        pollfd socket_pollfd;
        socket_pollfd.fd = m_socket_fd;

        socket_pollfd.events = POLLIN | POLLRDHUP | POLLERR;
        socket_pollfd.revents = 0;

        if (m_proto.tx_data_len() > 0)
        {
            socket_pollfd.events |= POLLOUT;
        }

        fds.push_back(socket_pollfd);

        // Add stdin if required.
        if (stdin_fd >= 0)
        {
            pollfd stdin_pollfd;
            stdin_pollfd.fd = stdin_fd;
            stdin_pollfd.events = POLLIN | POLLRDHUP | POLLERR;
            stdin_pollfd.revents = 0;

            fds.push_back(stdin_pollfd);
        }

        // Wait for something to happen.
        int rc = poll(fds.data(), fds.size(), -1);
        if (rc < 0)
            return errno;
        
        // What happened on the socket?
        size_t f = 0;
        if (fds[f].revents & POLLIN)
        {
            // Received some data on the socket.
            uint8_t read_buf[IO_BUF_SIZE];
            ssize_t read_bytes = read(m_socket_fd, &read_buf, sizeof(read_buf));
            if (read_bytes == -1)
                return errno;

            m_proto.add_rx_data(read_buf, read_bytes);

            // Try to decode a message.
            if (m_proto.decode(decoded_msg_type, decoded_msg))
                return 0;
        }

        if (fds[f].revents & POLLOUT)
        {
            // We can send some data on the socket.
            send_tx_buffer();
        }

        if (fds[f].revents & POLLHUP)
        {
            // The other end hung up.
            decoded_msg_type = Proto::MsgType::CONNECTION_CLOSED;
            return 0;
        }

        if (fds[f].revents & POLLERR)
        {
            // An error occurred on the socket.
            return ECOMM;
        }

        f++;

        // Did anything happen on stdin?
        if (stdin_fd >= 0)
        {
            if (fds[f].revents & POLLIN)
            {
                got_stdin = true;
            }

            f++;
        }
    }

    return 0;
}

} // namespace ggx
