//
// Utilities used by libggx.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#include <fcntl.h>

#include "ggxutil.hpp"



namespace ggx
{



//
// Make an fd non-blocking.
//

int set_nonblocking(int fd)
{
    // Get the terminal flags.
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        return errno;

    // If it's already non-blocking leave it alone.
    if (flags & O_NONBLOCK)
        return 0;

    // Set it to non-blocking.
    flags |= O_NONBLOCK;
    int rc = fcntl(fd, F_SETFL, flags);
    if (rc == -1)
        return errno;
    
    return 0;
}



} // namespace ggx
