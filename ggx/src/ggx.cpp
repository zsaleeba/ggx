//
// A client program which can be used to execute programs within the greengrass environment.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <cerrno>
#include <climits>
#include <cstring>
#include <unistd.h>

#include "ggxclient.hpp"



int main(int argc, char **argv, char **envp)
{
    // Check args.
    if (argc <= 1)
    {
        std::cout << 
            "ggx - executes a command inside the greengrass environment\n"
            "Format: ggx <command> <args>...\n";
        exit(EXIT_FAILURE);
    }

    // Gather args.
    std::string command = argv[1];

    std::vector<std::string> arg_vec;
    for (int i = 2; i < argc; i++)
    {
        arg_vec.push_back(argv[i]);
    }

    // Gather environment into a map.
    std::map<std::string, std::string> env_map;
    for (int i = 0; envp[i] != nullptr; i++)
    {
        std::string env_line(envp[i]);
        auto equals_pos = env_line.find('=');
        if (equals_pos != std::string::npos)
        {
            auto key = env_line.substr(0, equals_pos);
            auto value = env_line.substr(equals_pos+1);
            env_map[key] = value;
        }
    }

    // Get the current working directory.
    char cwd_buf[PATH_MAX];
    if (getcwd(cwd_buf, PATH_MAX) == nullptr)
    {
        std::cerr << "error in getcwd(): " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }

    // Run the command.
    ggx::Client ggxc;
    int rc = ggxc.exec(command, arg_vec, env_map, cwd_buf);
    if (rc != 0)
    {
        std::cerr << "can't execute: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }

    // Copy I/O across to stdin/stdout/stderr until the program terminates.
    int return_code = 0;
    rc = ggxc.copy_io(return_code);
    if (rc != 0)
    {
        std::cerr << "can't execute: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }

    return return_code;
}
