//
// Runs a program and encapsulates it under an execution environment so it
// looks like it's running from the command line.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#ifndef GGXSUBPROGRAM_HPP
#define GGXSUBPROGRAM_HPP

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <sys/types.h>
#include <unistd.h>


namespace ggx
{

// Forward declarations.
class ServerConn;


//
// Runs a program and encapsulates it under an execution environment so it
// looks like it's running from the command line.
//

class SubProgram
{
public:
    // Constructor.
    explicit SubProgram(ServerConn &conn, std::string &command, std::vector<std::string> &args, std::map<std::string, std::string> &env, std::string &pwd, bool isatty, std::function<void(int)> finished);
    virtual ~SubProgram();

    // Accessors.
    int stdin_fd() const  { return m_stdin_fd; }
    int stdout_fd() const { return m_stdout_fd; }
    int stderr_fd() const { return m_stderr_fd; }

private:
    // Start the program.
    int start();

private:
    // The parent ServerConn.
    ServerConn &m_conn;

    // Command parameters.
    std::string                        m_command;
    std::vector<std::string>           m_args;
    std::map<std::string, std::string> m_env;
    std::string                        m_pwd;
    bool                               m_isatty;
    std::function<void(int)>           m_finished;

    // The pid of the program we've fork()ed.
    pid_t m_pid;

    // fds for the sub-program we're connecting to.
    int m_stdin_fd;
    int m_stdout_fd;
    int m_stderr_fd;
};


} // namespace ggx



#endif // GGXSUBPROGRAM_HPP
