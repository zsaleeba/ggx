//
// A client class which can be used to execute programs within the greengrass environment.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#ifndef GGXSERVER_HPP
#define GGXSERVER_HPP

#include <string>
#include <memory>
#include <map>
#include <mutex>

#include "ggxserverconn.hpp"


namespace ggx
{


//
// A client class which can be used to execute programs within the greengrass environment.
//

class Server
{
private:
    static constexpr const size_t IO_BUF_SIZE = 65536;

public:
    // Constructor.
    explicit Server();
    virtual ~Server();

    // Starts the server listening.
    int start();

    // Runs the server, accepting connections and sending I/O.
    int run();

    // Remove a connection from the connection map, closing and forgetting it.
    void remove_connection(int conn_id);

private:
    // The socket connection to the ggserver.
    int m_server_fd;

    // A set of client connections.
    std::map< int, std::shared_ptr<ServerConn> > m_conns;
    std::mutex                                   m_conns_mutex;
};


} // namespace ggx


#endif // GGXSERVER_HPP
