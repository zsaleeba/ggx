#ifndef GGXSERVERCONN_HPP
#define GGXSERVERCONN_HPP

#include <string>
#include <thread>
#include <vector>
#include <map>
#include <functional>
#include <memory>
#include <cstdlib>

#include "ggxproto.hpp"


namespace ggx
{


// Forward declarations.
class Server;
class SubProgram;


//
// A socket connection and protocol for communicating to a client on the ggserver.
//

class ServerConn
{
private:
    // How many bytes we allow in the output queue before halting flow.
    static constexpr const size_t OUTPUT_QUEUE_HIGH_WATER = 65536;

#if 0
    //
    // Input messages.
    //

    class InputMessage
    {
    public:
        // Constructor for input messages representing signals.
        explicit InputMessage(int signal)
            : m_signal(signal)
        {}

        // Constructor for input messages representing stdin data.
        explicit InputMessage(std::string data)
            : m_signal(-1),
              m_data(data)
        {}

        bool               is_signal() const  { return m_signal >= 0; }
        int                signal() const     { return m_signal; }
        const std::string &stdin_data() const { return m_data; }

    private:
        int         m_signal;
        std::string m_data;
    };
#endif
    
    //
    // Output messages.
    //

    class OutputMessage
    {
    public:
        // Constructor.
        explicit OutputMessage(std::string data, bool is_stderr)
            : m_data(data),
              m_is_stderr(is_stderr)
        {}

        bool               is_stderr() const { return m_is_stderr; }
        const std::string &data() const      { return m_data; }

    private:
        bool        m_is_stderr;
        std::string m_data;
    };


    //
    // Actions to take on input.
    //
    enum class InputAction
    {
        SOCKET,
        SUBPROGRAM_STDIN,
        SUBPROGRAM_STDOUT,
        SUBPROGRAM_STDERR
    };

public:
    // The size of the I/O buffer.
    static constexpr const size_t IO_BUF_SIZE = 65536;

public:
    // Constructor.
    explicit ServerConn(Server &server, int socket_fd, int conn_id);
    virtual ~ServerConn();

    // Close the socket.
    void close_socket();

    // Send messages.
    int send_exec_response();
    int send_stdout_data(const std::string &data);
    int send_stderr_data(const std::string &data);
    int send_program_terminated(int return_code);

    // Send and receive I/O. Returns when a message is received.
    int receive_message(Proto::MsgType &decoded_msg_type, std::vector<uint8_t> &decoded_msg, int stdin_fd, bool &got_stdin);

    // Decode a program terminated message.
    bool decode_exec_request(const std::vector<uint8_t> &msg_data, std::string &command, std::vector<std::string> &args, std::map<std::string, std::string> &env, std::string &pwd) { return m_proto.decode_exec_request(msg_data, command, args, env, pwd); }

private:
    // Transfer data to/from the connection and act on it.
    void process_io();
    
    // Send the transmit buffer.
    int send_tx_buffer();

    // Receive from the socket.
    void socket_receive();

    // The number of bytes of data in the output queue.
    size_t output_queue_space_used() const;

private:
    // The parent Server.
    Server &m_server;

    // The socket connection to the client.
    int     m_socket_fd;

    // The connection id which is used when deleting connections.
    int     m_conn_id;

    // The program we're running.
    std::unique_ptr<SubProgram> m_sub_program;

    // Input message queue.
    std::vector<uint8_t>        m_input_queue;

    // Output message queue.
    std::vector<OutputMessage>  m_output_queue;

    // I/O processing runs as a thread which is created by the constructor.
    std::thread                 m_io_thread;

    // The protocol.
    Proto   m_proto;
};


} // namespace ggx



#endif // GGXSERVERCONN_HPP
