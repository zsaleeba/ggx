//
// Main program for the ggx server. Accepts connections from the
// ggx client and allows command line programs to run under the 
// greengrass environment.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-14
//

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <cerrno>
#include <climits>
#include <cstring>
#include <cerrno>
#include <unistd.h>

#include "ggxserver.hpp"



int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
    std::cout << "ggxserver starting.\n" << std::flush;

    // Start the server listening.
    ggx::Server server;
    int rc = server.start();
    if (rc != 0)
    {
        std::cerr << "error starting: " << strerror(errno) << std::endl << std::flush;
        exit(EXIT_FAILURE);
    }

    // Process connections.
    rc = server.run();
    if (rc != 0)
    {
        std::cerr << "error running server: " << strerror(errno) << std::endl << std::flush;
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}
