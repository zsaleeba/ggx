//
// Runs a program and encapsulates it under an execution environment so it
// looks like it's running from the command line.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-14
//

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <sys/types.h>
#include <unistd.h>

#include "ggxserverconn.hpp"
#include "ggxsubprogram.hpp"


namespace ggx
{


//
// Constructor.
//

SubProgram::SubProgram(ServerConn &conn, std::string &command, std::vector<std::string> &args, std::map<std::string, std::string> &env, std::string &pwd, std::function<void(int)> finished)
    : m_conn(conn),
      m_command(std::move(command)),
      m_args(std::move(args)),
      m_env(std::move(env)),
      m_isatty(isatty),
      m_finished(finished)
{
}


SubProgram::~SubProgram()
{
}


} // namespace ggx
