//
// A socket connection and protocol for communicating to a client on the ggserver.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#include <iostream>
#include <string>
#include <thread>
#include <cerrno>
#include <cstring>
#include <unistd.h>
#include <poll.h>

#include "ggxserverconn.hpp"
#include "ggxsubprogram.hpp"
#include "ggxproto.hpp"


namespace ggx
{


//
// Constructor.
//

ServerConn::ServerConn(Server &server, int socket_fd, int conn_id)
    : m_server(server),
      m_socket_fd(socket_fd),
      m_conn_id(conn_id)
{
    m_io_thread = std::move(std::thread(&ServerConn::process_io, this));
}


ServerConn::~ServerConn()
{
    // Close the socket.
    close_socket();

    // Reap the thread.
    if (m_io_thread.joinable())
    {
        m_io_thread.join();
    }
}


//
// Close the socket.
//

void ServerConn::close_socket()
{
    if (m_socket_fd >= 0)
    {
        ::close(m_socket_fd);
        m_socket_fd = -1;
    }
}


//
// Transfer data to/from the connection and act on it.
// Transfer stdin data from the protocol to the sub-program.
// Transfer stdout and stderr data from the sub-program to the protocol.
// Transfer termios from the sub-program to the protocol.
//

void ServerConn::process_io()
{
    // Buffers for data.
    std::string stdin_data;
    std::string stdout_data;
    std::string stderr_data;
    int         signal_id = -1;

    // Keep getting data until we have a packet.
    while (true)
    {
        // Encode stdout and stderr from the output queue and if there's room in the tx buffer.
        while (!m_output_queue.empty() && m_proto.tx_data_len() < Proto::TX_BUF_HIGH_WATER)
        {
            auto &msg = m_output_queue.front();
            if (msg.is_stderr())
            {
                m_proto.encode_stderr_data(msg.data());
            }
            else
            {
                m_proto.encode_stdout_data(msg.data());
            }

            m_output_queue.erase(m_output_queue.begin());
        }

        // What events are we waiting on?
        std::vector<pollfd>      fds;
        std::vector<InputAction> fd_actions;

        // We're always interested in messages from the socket.
        pollfd socket_pollfd;
        socket_pollfd.fd      = m_socket_fd;
        socket_pollfd.events  = POLLRDHUP | POLLERR;
        socket_pollfd.revents = 0;

        if (m_proto.tx_data_len() > 0)
        {
            socket_pollfd.events |= POLLOUT;
        }

        fds.push_back(socket_pollfd);
        fd_actions.push_back(InputAction::SOCKET);

        // If we have space in the output message queue keep an eye on the 
        // sub-program's stdout and stderr.
        if (output_queue_space_used() < OUTPUT_QUEUE_HIGH_WATER)
        {

            pollfd stdout_pollfd;
            socket_pollfd.fd      = m_sub_program->stdout_fd();
            socket_pollfd.events  = POLLIN | POLLRDHUP | POLLERR;
            socket_pollfd.revents = 0;
            fds.push_back(stdout_pollfd);
            fd_actions.push_back(InputAction::SUBPROGRAM_STDOUT);

            pollfd stderr_pollfd;
            socket_pollfd.fd      = m_sub_program->stderr_fd();
            socket_pollfd.events  = POLLIN | POLLRDHUP | POLLERR;
            socket_pollfd.revents = 0;
            fds.push_back(stderr_pollfd);
            fd_actions.push_back(InputAction::SUBPROGRAM_STDERR);
        }

        // If we've received some stdin from the socket check if we can send it to the sub-program.
        if (!m_input_queue.empty())
        {
            pollfd stdin_pollfd;
            stdin_pollfd.fd = m_sub_program->stdin_fd();
            stdin_pollfd.events = POLLOUT | POLLERR;
            stdin_pollfd.revents = 0;

            fds.push_back(stdin_pollfd);
            fd_actions.push_back(InputAction::SUBPROGRAM_STDIN);
        }

        // Wait for something to happen.
        int rc = poll(fds.data(), fds.size(), -1);
        if (rc < 0)
        {
            std::cout << "ServerConn::process_io() poll error: " << strerror(errno) << std::endl << std::flush;
        }
        else
        {
            // Check all the fds.
            for (int i = 0; i < fds.size(); i++)
            {
                // Was there an error?
                if (fds[i].revents & (POLLERR | POLLHUP))
                {
                    // Close the socket and kill the program.
                    XXX;
                }

                // Handle each socket type.
                switch (fd_actions[i])
                {
                case InputAction::SOCKET:
                    // Is there socket input?
                    if (fds[i].revents & POLLIN)
                    {
                        socket_receive();
                    }

                    // Can we write to the socket?
                    if (fds[i].revents & POLLOUT)
                    {
                        send_tx_buffer();
                    }
                    break;

                case InputAction::SUBPROGRAM_STDIN:
                    break;

                case InputAction::SUBPROGRAM_STDOUT:
                    break;

                case InputAction::SUBPROGRAM_STDERR:
                    break;
                }
            }

            size_t f = 0;
            if (fds[f].revents & POLLIN)
            {
                // Received some data on the socket.
                uint8_t read_buf[IO_BUF_SIZE];
                ssize_t read_bytes = read(m_socket_fd, &read_buf, sizeof(read_buf));
                if (read_bytes == -1)
                    return errno;

                m_proto.add_rx_data(read_buf, read_bytes);

                // Try to decode a message.
                if (m_proto.decode(decoded_msg_type, decoded_msg))
                    return 0;
            }

            if (fds[f].revents & POLLOUT)
            {
                // We can send some data on the socket.
                send_tx_buffer();
            }

            if (fds[f].revents & POLLHUP)
            {
                // The other end hung up.
                decoded_msg_type = Proto::MsgType::CONNECTION_CLOSED;
                return 0;
            }

            if (fds[f].revents & POLLERR)
            {
                // An error occurred on the socket.
                return ECOMM;
            }

            f++;

            // Did anything happen on stdin?
            if (stdin_fd >= 0)
            {
                if (fds[f].revents & POLLIN)
                {
                    got_stdin = true;
                }

                f++;
            }
        }
    }

    return 0;
}


//
// Send the transmit buffer.
// Returns 0 on success or an errno on error.
//

int ServerConn::send_tx_buffer()
{
    ssize_t written = write(m_socket_fd, m_proto.tx_data(), m_proto.tx_data_len());
    if (written < 0)
        return errno;

    if (written > 0)
    {
        m_proto.pop_tx_data(written);
    }

    return 0;
}


//
// Send messages.
//

int ServerConn::send_exec_response()
{
    m_proto.encode_exec_response();
    return send_tx_buffer();    
}


int ServerConn::send_stdout_data(const std::string &data)
{
    m_proto.encode_stdout_data(data);
    return send_tx_buffer();    
}


int ServerConn::send_stderr_data(const std::string &data)
{
    m_proto.encode_stderr_data(data);
    return send_tx_buffer();    
}


int ServerConn::send_program_terminated(int return_code)
{
    m_proto.encode_program_terminated(return_code);
    return send_tx_buffer();    
}


//
// Receive data from the client socket and interpret it.
//

void ServerConn::socket_receive()
{
    int                                signal_id;
    std::string                        command;
    std::vector<std::string>           args;
    std::map<std::string, std::string> env;
    std::string                        pwd;
    bool                               isatty;

    // Received some data on the socket.
    uint8_t read_buf[IO_BUF_SIZE];
    ssize_t read_bytes = read(m_socket_fd, &read_buf, sizeof(read_buf));
    if (read_bytes == -1)
    {
        std::cout << "client socket read error: " << strerror(errno) << std::endl << std::flush;
    }
    else
    {
        // Add the new data.
        m_proto.add_rx_data(read_buf, read_bytes);

        // Try to decode messages.
        Proto::MsgType       decoded_msg_type = Proto::MsgType::NONE;
        std::vector<uint8_t> decode_data;

        while (m_proto.decode(decoded_msg_type, decode_data))
        {
            switch (decoded_msg_type)
            {
                case Proto::MsgType::EXEC_REQ:
                    if (!m_sub_program && m_proto.decode_exec_request(decode_data, command, args, env, pwd, isatty, std::bind(&ServerConn::program_finished, this)))
                    {
                        // Execute the sub-program.
                        m_sub_program = std::make_unique<SubProgram>(command, args, env, pwd, isatty);
                    }
                    break;

                case Proto::MsgType::STDIN_DATA:
                    // Add it to the input queue.
                    m_input_queue.insert(m_input_queue.end(), decode_data.begin(), decode_data.end());
                    break;

                case Proto::MsgType::RAISE_SIGNAL:
                    // Raise the signal.
                    if (m_sub_program && m_proto.decode_raise_signal(decode_data, signal_id))
                    {
                        m_sub_program->raise_signal(signal_id);
                    }
                    break;

                case Proto::MsgType::NONE:
                case Proto::MsgType::EXEC_RESP:
                case Proto::MsgType::STDOUT_DATA:
                case Proto::MsgType::STDERR_DATA:
                case Proto::MsgType::TERMIO_SETTING:
                case Proto::MsgType::PROGRAM_TERMINATED:
                case Proto::MsgType::CONNECTION_CLOSED:
                    // Ignore these.
                    break;
            }
        }
    }
}


//
// Count the number of bytes in the output queue.
//

size_t ServerConn::output_queue_space_used() const
{
    size_t total = 0;

    for (auto &q: m_output_queue)
    {
        total += q.data().size();
    }

    return total;
}



} // namespace ggx
