//
// Listens on the ggx server port and accepts connections.
//
// Zik Saleeba <zik@zikzak.net>   2022-05-07
//

#include <string>
#include <vector>
#include <map>
#include <cerrno>
#include <unistd.h>
#include <netinet/in.h>
#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <sys/socket.h>

#include "ggxserver.hpp"
#include "ggxclientconn.hpp"


namespace ggx
{


//
// Constructor.
//

Server::Server()
    : m_server_fd(-1)
{
}


Server::~Server()
{
    {
        std::lock_guard<std::mutex> lock(m_conns_mutex);

        // Close all the connections.
        for (auto conn: m_conns)
        {
            conn.second->close_socket();
        }
    }

    // Close the server port.
    if (m_server_fd >= 0)
    {
        ::close(m_server_fd);
    }
}


//
// Starts the server listening.
// Returns 0 on success or an errno on error.
//

int Server::start()
{
    struct sockaddr_in address;
 
    // Create the server socket.
    m_server_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if (m_server_fd == -1)
        return errno;
 
    // Reuse the port so we don't fail when the port is in timeout state.
    int enable = 1;
    if (setsockopt(m_server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enable, sizeof(enable)) == -1) 
        return errno;

    // Create the address for our local port.
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(ClientConn::GGX_SERVER_PORT);
 
    // Bind to the port.
    if (bind(m_server_fd, (struct sockaddr*)&address, sizeof(address)) == -1) 
        return errno;

    // Listen for connections on the port.
    if (listen(m_server_fd, 5) == -1)
        return errno;
    
    return 0;
}


//
// Runs the server, accepting connections and sending I/O.
// Returns 0 on success or an errno on error.
//

int Server::run()
{
    int next_conn_id = 1;

    while (true)
    {
        // Accept a connection.
        int conn_fd = accept(m_server_fd, nullptr, nullptr);
        if (conn_fd == -1)
            return errno;

        // Create the ServerConn.
        auto conn = std::make_shared<ServerConn>(*this, conn_fd, next_conn_id);

        // Add it to the map.
        std::lock_guard<std::mutex> lock(m_conns_mutex);
        m_conns[next_conn_id] = conn;
        next_conn_id++;
    }

    return 0;
}


//
// Remove a connection from the connection map, closing and forgetting it.
//

void Server::remove_connection(int conn_id)
{
    std::lock_guard<std::mutex> lock(m_conns_mutex);
    m_conns.erase(conn_id);
}


} // namespace ggx
